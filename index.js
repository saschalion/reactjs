require('file?name=index.html!./index.html');
require('./img/favicon.ico');
require('./sass/style.sass');

const Main = require('./ts/Main.tsx');
const ReactDOM = require('react-dom');

ReactDOM.render(Main.createRouter(), document.getElementById('main'));