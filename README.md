ReactJs demo page
-----------

Установка и использование
-------------------------

  - npm install
  - tsd install es6-promise react react-dom react-router classnames react-addons-update -ros (не надо)


В `package.json` есть два скрипта для сборки проекта:

  - `npm run build` - собирает весь проект в папочку `dist`
  - `npm run dev` - запускает вебсервер на порту 8080