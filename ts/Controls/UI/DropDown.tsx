import classes = require('classnames');
import KeyboardEvent = __React.KeyboardEvent;
import React = require('react');
import ReactDOM = require('react-dom');

import { KeyCodes } from '../../Types';
import { Button, ButtonStyles } from '../Button';

export interface DropDownItemData
{
	id: string;
	caption: string;
	tag?: string; // Передаётся цвет
	iconClass?: string;
	disabled?: boolean;
	subTitle?: string; // Подзаголовок
	group?: string;
}

interface DropDownItemProps extends React.Props<Item>
{
	focused?: boolean;
	node: DropDownItemData;
	onSelect: (item: DropDownItemData) => void;
	onEnter: (item: DropDownItemData) => void;
	disabled?: boolean;
	selected?: boolean;
	searchString?: string;
}

class Item extends React.Component<DropDownItemProps, void>
{
	private onItemClick(e)
	{
		if ( this.props.disabled ) return;
		this.props.onEnter(this.props.node);
	}

	renderTag(): React.ReactElement<any>
	{
		let node = this.props.node;

		if ( node.tag ) {
			return(
				<span
					className="ui-dropdown__tag"
					style={ {
						backgroundColor: node.tag
					} }
				/>
			);
		} else {
			return null;
		}
	}

	renderIcon(): React.ReactElement<any>
	{
		let node = this.props.node;

		if ( node.iconClass ) {
			return(
				<span className={ classes('ui-dropdown__icon', node.iconClass) } />
			);
		} else {
			return null;
		}
	}

	renderCaption(): JSX.Element
	{
		let titleChildren = [];
		let caption = this.props.node.caption;

		if ( !!this.props.searchString ) {
			let regex = new RegExp(this.props.searchString, 'gi' );
			let segments = caption.split(regex);
			let replacements  = caption.match(regex);

			segments.forEach(function(segment, i) {
				segment && titleChildren.push(
					<span className="ui-dropdown__segment" key={ i + 'segment' }>
						{ segment }
					</span>
				);

				if ( segments.length === i + 1) {
					return;
				}

				titleChildren.push(
					<span className="ui-dropdown__segment _state_highlight" key={ i + 'highlight' }>
						{ replacements[i] }
					</span>
				);
			}.bind(this));

			return(
				<span className="ui-dropdown__caption">
					{ titleChildren }
				</span>
			)
		} else {
			return(
				<span className="ui-dropdown__caption">
					{ caption }
				</span>
			);
		}
	}

	renderSubTitle(): JSX.Element
	{
		let titleChildren = [];
		let caption = this.props.node.subTitle;

		if ( !!this.props.searchString ) {
			let regex = new RegExp(this.props.searchString, 'gi' );
			let segments = caption.split(regex);
			let replacements  = caption.match(regex);

			segments.forEach(function(segment, i) {
				segment && titleChildren.push(
					<span className="ui-dropdown__segment" key={ i + 'segment' }>
						{ segment }
					</span>
				);

				if ( segments.length === i + 1) {
					return;
				}

				titleChildren.push(
					<span className="ui-dropdown__segment _state_highlight" key={ i + 'highlight' }>
						{ replacements[i] }
					</span>
				);
			}.bind(this));

			return(
				<span className="ui-dropdown__sub-title">
					{ titleChildren }
				</span>
			)
		} else {
			return(
				<span className="ui-dropdown__sub-title">
					{ caption }
				</span>
			);
		}
	}

	render(): JSX.Element
	{
		let props = this.props;
		let selected = props.selected;

		let itemClasses =
			classes(
				'ui-dropdown__item',
				selected && '_state_selected',
				props.selected && '_state_focused',
				this.props.disabled && '_state_disabled'
			);

		return(
			<div
				className={ itemClasses }
				onClick={ (e) => this.onItemClick(e) }
			>
				{ this.renderTag() }
				{ this.renderIcon() }
				{ this.renderCaption() }

				{ props.node.subTitle && this.renderSubTitle() }
			</div>
		);
	}
}

interface ItemGroupProps extends React.Props<ItemGroup>
{
	caption: string;
	searchString?: string;
}

class ItemGroup extends React.Component<ItemGroupProps, void>
{
	renderCaption(): JSX.Element
	{
		let titleChildren = [];
		let caption = this.props.caption;

		if ( !!this.props.searchString ) {
			let regex = new RegExp(this.props.searchString, 'gi' );
			let segments = caption.split(regex);
			let replacements  = caption.match(regex);

			segments.forEach(function(segment, i) {
				segment && titleChildren.push(
					<span className="ui-dropdown__segment" key={ i + 'segment' }>
						{ segment }
					</span>
				);

				if ( segments.length === i + 1) {
					return;
				}

				titleChildren.push(
					<span className="ui-dropdown__segment _state_highlight" key={ i + 'highlight' }>
						{ replacements[i] }
					</span>
				);
			}.bind(this));

			return(
				<span className="ui-dropdown__caption">
					{ caption }
				</span>
			)
		} else {
			return(
				<span className="ui-dropdown__caption">
					{ caption }
				</span>
			);
		}
	}

	render(): React.ReactElement<any>
	{
		return(
			<div className="ui-dropdown__item _type_group _type_normal">
				{ this.renderCaption() }
			</div>
		);
	}
}


interface DropDownProps extends React.Props<DropDown>
{
	id?: string; // ID выбранного элемента
	data: DropDownItemData[];
    onChange: (id: string) => void;
	caption?: string;
	description?: string;
	errorText?: string;
	required?: boolean;
	hasError?: boolean;
	showRoot?: boolean; // Отображать корневой элемент?
	hasClearButton?: boolean; // Отображать кнопку очистки выбранного элемента до корневой директории
	type?: 'toolbar';
	placeholder?: string;
	disabled?: boolean;
	customSortData?: (data: DropDownItemData[]) => DropDownItemData[];
	extraClass?: string; // Дополнительный CSS-класс
	showSubTitleInCaption?: boolean;

	hasSearch?: boolean; // Использовать поиск для фильтрации списка
	searchPlaceholder?: string; // Дефолтный текст для поля поиска
	size?: 'large';
	hiddenField?: boolean; // Не отображать поле

	buttonIconClass?: string; // Иконка для кнопки раскрытия списка
	buttonStyle?: ButtonStyles;
	buttonHollow?: boolean; // Кнопка с бесцветным фоном и обводкой
	hiddenElementsForEmptySearch?: boolean;
	buttonText?: string;
}

interface DropDownState
{
	data?: DropDownItemData[];
	initialData?: DropDownItemData[];
	popup?: boolean;
	selectedId?: string;
	searchFocused?: boolean;
	searchString?: string;
	searchMode?: boolean;
}

export class DropDown extends React.Component<DropDownProps, DropDownState>
{
	private _selectedItemIndex: number = -1;
	private _itemsOffset: number = 0;
	private _mounted: boolean;
	private _currentItem: DropDownItemData;
	private _focusedItem: DropDownItemData;
	private _dropDownID: string;
	private _searchTimer: number;
	private _searchTimerDelay: number = 300; // Задержка в мс для поиска

	constructor(props)
	{
		super(props);

		this.state = {
			data: props.data,
			initialData: props.data,
			popup: false,
			selectedId: props.id,
			searchFocused: false,
			searchString: ''
		};

		this._dropDownID = 'popup-drop-down-' + Math.random();
	}

	componentDidMount()
	{
		this._mounted = true;
		this.searchNode(this.props.id);
	}

	componentWillReceiveProps(nextProps)
	{
		if ( this.props.id != nextProps.id || nextProps.id == null || nextProps.data.length != this.state.data.length ) {
			this.setState({
				data: nextProps.data,
				initialData: nextProps.data
			}, () => this.searchNode(nextProps.id));
		}
	}

	componentWillUnmount()
	{
		this._mounted = false;
	}

	setFocus()
	{
		setTimeout(() => {
			let elem = (ReactDOM.findDOMNode(this.refs['list-field']) as HTMLElement);
			elem && elem.focus();
		}, 50);
	}

	private searchFocus()
	{
		let elem = (ReactDOM.findDOMNode(this.refs['search-field']) as HTMLElement);
		elem && elem.focus();
	}

	private searchNode(id: string)
	{
		if ( id == null ) {
			this._currentItem = null;
			this._focusedItem = null;
			this._selectedItemIndex = -1;
			this.setState({});
		} else {
			if ( !id ) {
				id = '';
			}

			if ( this.state.data.length && id ) {
				this.sortData().forEach((item: DropDownItemData, index) => {
					if ( item.id == id ) {
						this._currentItem = item;
						this._selectedItemIndex = index;

						this.setState({});
					}
				});
			}
		}
	}

	private setPopup(flag: boolean)
	{
		if ( this.state.popup == flag || this.props.disabled ) return;

		if ( flag ) {
			document.addEventListener('click', this.onDocumentClick);
		} else {
			document.removeEventListener('click', this.onDocumentClick);
		}

		this.setState({
			popup: flag
		}, () => {
			if ( flag ) {
				this.setState({
					searchString: ''
				});

				this.loadData();
				this.searchFocus();
				this.scrollToSelectedItem();
			}
		});
	}

	private loadData(subString = '')
	{
		let data = this.state.initialData;

		if ( subString ) {
			data = this.search(data, subString);
		} else {
			data = this.props.data;
		}

		this.setState({
			data: data
		}, () => {
			this.setFocusedItemIndex();
		});
	}

	private scrollToSelectedItem()
	{
		if ( this._focusedItem ) {
			let node = this._focusedItem;
			let items = ReactDOM.findDOMNode(this.refs['items']) as HTMLElement;
			let item = ReactDOM.findDOMNode(this.refs['item-' + node.id]) as HTMLElement;
			let searchHeight = 0;

			if ( this.props.hasSearch ) {
				let searchElem = ReactDOM.findDOMNode(this.refs['search-inner']) as HTMLElement;

				if ( searchElem ) {
					searchHeight = searchElem.clientHeight + 1;
				}
			}

			if ( !item ) return;

			let top = item.offsetTop - (items.scrollTop + this._itemsOffset) - searchHeight;
			let bottom = items.offsetHeight - (item.offsetTop + item.offsetHeight - items.scrollTop) + searchHeight;

			if ( top < 0 ) {
				items.scrollTop += top;
			} else if ( bottom < 0 ) {
				items.scrollTop -= bottom;
			}
		}
	}

	private search(data: DropDownItemData[], subString: string)
	{
		data = data.filter((item) => {
			if ( !subString ) return;

			let searchByCaption = (item.caption.toUpperCase().indexOf(subString.toUpperCase()) >= 0);
			let searchBySubTitle = false;

			if ( item.subTitle ) {
				searchBySubTitle = (item.subTitle.toUpperCase().indexOf(subString.toUpperCase()) >= 0)
			}

			return searchByCaption || searchBySubTitle;
		});

		return data;
	}

	private setFocusedItemIndex()
	{
		if ( !this.props.id ) {
			return;
		}

		let data = this.state.data;

		if ( data.length ) {
			for(let i = 0; i < data.length; i++) {
				if ( data[i].id != this.props.id ) {
					continue;
				}

				this._selectedItemIndex = i;
				this._currentItem = data[i];
			}
		}

		this.setState({});
	}

	private setButtonFocus()
	{
		(ReactDOM.findDOMNode(this.refs['button']) as HTMLElement).focus();
	}

	private onEscape()
	{
		this.setPopup(false);
	}

	private onKeyDown(event: KeyboardEvent)
	{
		if ( !this.state.popup ) return;

		let keyCode = event.keyCode;
		let selectedItemIndex = this._selectedItemIndex;

		switch (keyCode) {
			case KeyCodes.UP:
			{
				event.preventDefault();
				selectedItemIndex -= 1;
				let prevNode = this.state.data[selectedItemIndex];

				if ( prevNode ) {
					this._focusedItem = prevNode;
					this._selectedItemIndex = selectedItemIndex;

					this.scrollToSelectedItem();
					this.setState({});
				}

				break;
			}

			case KeyCodes.DOWN:
			{
				event.preventDefault();
				selectedItemIndex += 1;
				let nextNode = this.state.data[selectedItemIndex];

				if ( nextNode ) {
					this._focusedItem = nextNode;
					this._selectedItemIndex = selectedItemIndex;

					this.scrollToSelectedItem();
					this.setState({});
				}

				break;
			}

			case KeyCodes.ENTER:
			{
				this.onItemEnter(this._focusedItem, selectedItemIndex);
				event.stopPropagation();
				event.preventDefault();

				break;
			}

			case KeyCodes.TAB:
			{
				event.preventDefault();
				break;
			}

			case KeyCodes.ESC:
			{
				this.onEscape();
				event.preventDefault();
				event.stopPropagation();

				break;
			}
		}
	}

	onDocumentClick = (event) => {
		if ( !this._mounted ) {
			document.removeEventListener('click', this.onDocumentClick);
			return;
		}

		let e = event.target || event.srcElement;

		if ( e.id == this._dropDownID ) return;

		while (e = e.parentNode) {
			if ( e.id == this._dropDownID ) return;
		}

		this.setPopup(false);
	};

	private onChange(event)
	{
		let value = event['target']['value'];
		let searchMode = false;

		clearTimeout(this._searchTimer);

		this._searchTimer = setTimeout(() => {
			if ( value.length ) {
				this._selectedItemIndex = -1;
				searchMode = true;
			} else {
				searchMode = false;
			}

			this.setState({
				searchMode: searchMode,
				searchString: value
			});

			this.loadData(value);
		}, this._searchTimerDelay);
	}

	private onItemSelect(item: DropDownItemData, selectedItemIndex)
	{
		this._focusedItem = item;
		this._selectedItemIndex = selectedItemIndex;
		this.setFocus();
		this.setState({});
	}

	private onItemEnter(item: DropDownItemData, selectedItemIndex)
	{
		let currentNode = item;

		// Если выбранный элемент совпадает с редактируемым элементов в форме, то ничего не делаем
		if ( currentNode && currentNode.disabled ) return;

		this.onItemSelect(currentNode, selectedItemIndex);

		this._currentItem = item;
		this.props.onChange(currentNode.id);

		this.setButtonFocus();
		this.setPopup(false);
	}

	private sortData(): DropDownItemData[]
	{
		if ( this.props.customSortData ) {
			return this.props.customSortData(this.state.data);
		} else {
			return (this.state.data || []).sort((item1, item2) => {
				if ( item1.group && item2.group ) {
					if ( item1.group < item2.group ) {
						return -1;
					}

					if ( item1.group > item2.group ) {
						return 1;
					}
				}

				if ( item1.caption < item2.caption ) {
					return -1;
				}

				if ( item1.caption > item2.caption ) {
					return 1;
				}

				return 0;
			});
		}
	}

	private renderItems(): JSX.Element
	{
		if ( this.props.hiddenElementsForEmptySearch && !this.state.searchString ) {
			return;
		}

		let group: string;
		let items = [];

		this.sortData().forEach((item: DropDownItemData, index) => {
			if ( !!item.group && item.group != group ) {
				group = item.group;

				items.push(
					<ItemGroup
						caption={ item.group }
						searchString={ this.state.searchString }
					/>
				);
			}

			items.push(
				<Item
					node={ item }
					key={ item.id }
					selected={ index === this._selectedItemIndex }
					onSelect={ (item) => this.onItemSelect(item, index) }
					onEnter={ (item) => this.onItemEnter(item, index) }
					disabled={ item.disabled }
					ref={ 'item-' + item.id }
					searchString={ this.state.searchString }
				/>
			);
		});

		return(
			<div className="ui-dropdown__items">
				{ items.length ? items : this.renderEmptyItem() }
			</div>
		);
	}

	private renderEmptyItem(): JSX.Element
	{
		return(
			<div className="ui-dropdown__item _type_normal">
				{ 'Не найдено' }
			</div>
		);
	}

	private renderSearch(): JSX.Element
	{
		return(
			<div ref="search-inner"
				 className={ classes('ui-dropdown__field-inner', this.state.searchFocused && '_state_focused') }
			>
				<input
					className="ui-dropdown__field ui__field _size_small"
					placeholder={ this.props.searchPlaceholder ? this.props.searchPlaceholder : 'Начните вводить' }
					ref="search-field"
					onChange={ (e: KeyboardEvent) => this.onChange(e) }
					onFocus={ () => {
							this.setState({
								searchFocused: true
							});
						}
					}
					onBlur={ () => {
							this.setState({
								searchFocused: false
							});
						}
					}
				/>

				<span className="ui-dropdown__search fa fa-search" />
			</div>
		);
	}

	private renderDropDown(): JSX.Element
	{
		return(
			<div className={ classes('ui-dropdown', this.state.popup && '_state_expanded') }>
				{ this.props.hasSearch && this.renderSearch() }
				<div className="ui-dropdown__items-inner" ref="items">
					{ this.renderItems() }
				</div>
			</div>
		);
	}

	private clearCurrentItem()
	{
		this._currentItem = null;
		this._focusedItem = null;

		this.props.onChange(null);
		this._selectedItemIndex = -1;

		if ( this.state.popup ) {
			this.setPopup(false);
		}

		this.setButtonFocus();
		this.setState({});
	}

	private renderClearButton(): JSX.Element
	{
		if ( !this._currentItem || this.props.disabled ) {
			return null;
		}

		return(
			<a
				className="ui-dropdown__clear"
				tabIndex={ 0 }
				onKeyDown={ (e: KeyboardEvent) => {
					if ( e.keyCode == KeyCodes.ENTER ) {
						this.clearCurrentItem();
					}
				} }
	   			onClick={ () => this.clearCurrentItem() }
			>
				<i className="fa fa-times" />
			</a>
		);
	}

	private setPlaceholder(): string
	{
		let caption = '';
		let props = this.props;

		if ( this._currentItem ) {
			caption += this._currentItem.caption;
            caption += (props.showSubTitleInCaption) ? (' (' + this._currentItem.subTitle + ')') : '';
        } else {
			caption = this.props.placeholder ? this.props.placeholder : 'Нет записей';
		}

		return caption;
	}

	private getButtonIcon(): string
	{
		if ( this.props.buttonIconClass ) {
			return this.props.buttonIconClass;
		} else {
			return this.state.popup ? 'fa fa-caret-up' : 'fa fa-caret-down';
		}
	}

	render(): JSX.Element
	{
		let props = this.props;

		return(
			<div className={
				classes(
					props.type == 'toolbar' ? 'toolbar__item' : 'ui__label',
					this.state.popup && '_state_expanded'
				)
			}>
				<div
					className={
                        classes(
                            'ui-dropdown__wrap',
                            props.type && '_type_' + props.type,
                            props.hasClearButton && '_has-clear-btn',
                            props.size && '_size_large',
                            props.hiddenField && '_hidden-field',
                            props.extraClass
                        )
                    }
					id={ this._dropDownID }
					onKeyDown={ (e: KeyboardEvent) => this.onKeyDown(e) }
				>
					<div className="ui-dropdown__fields">
						{
							this.props.caption &&
								<span
									className="ui__label-text"
									key={ 0 }
								>
									{ this.props.caption }
									{
										props.required &&
										<span className="ui__required">{ ' *' }</span>
									}
								</span>
						}

						<div
							className="ui__field-wrap"
							key={ 1 }
						>
							<div className="ui__field-inner">
								{
									!props.hiddenField &&
									<div
										className={
										classes(
											'ui-dropdown__main-field ui__field',
											!this._currentItem && '_state_empty',
											props.hasError && '_state_error',
											props.disabled && '_state_readonly'
										)
									}
										tabIndex={ 0 }
										ref="list-field"
										onClick={ () => this.setPopup(!this.state.popup) }
										onKeyDown={ (e) => {
										if ( e.keyCode == KeyCodes.ENTER ) {
											this.setPopup(!this.state.popup);
										}
									} }
									>
										{ this.setPlaceholder() }
									</div>
								}

								{ props.hasClearButton && this.renderClearButton() }

								<Button
									icon={ !props.buttonText && this.getButtonIcon() }
									action={ () => this.setPopup(!this.state.popup) }
									onKeyDown={ (e: KeyboardEvent) => {
											if ( e.keyCode == KeyCodes.ENTER ) {
												if ( !this.state.popup ) {
													this.setPopup(true);
													e.stopPropagation();
												}
											}
										}
									}
									ref="button"
									style={ props.buttonStyle ? props.buttonStyle : 'blue' }
									disabled={ props.disabled }
									dropDown={ true }
									hollow={ props.buttonHollow }
									extraClass={ props.hiddenField && '_position_static' }
									text={ props.buttonText }
								/>

								{ this.state.popup && this.renderDropDown() }
							</div>

							{
								!!props.description &&
								<span className="ui__desc">
									{ props.description }
								</span>
							}

							{
								(!!props.errorText && props.hasError) &&
								<span className="ui__error">
									{ props.errorText }
								</span>
							}
						</div>
					</div>
				</div>
			</div>
		);
	}
}