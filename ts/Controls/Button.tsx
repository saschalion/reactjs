import classNames = require('classnames');
import React = require('react');
import ReactDOM = require('react-dom');
import SyntheticEvent = __React.SyntheticEvent;

import { Link } from 'react-router';

export type ButtonStyles = 'default' | 'green' | 'orange' | 'red' | 'blue' | 'black';

// Button Control
export interface ButtonProps extends React.Props<Button>
{
    text?: string; // Текст на кнопке
    icon?: string; // Название класса / классов для икони, например Font Awesome - fa fa-database
    iconColor?: string;
    iconPosition?: 'right';
    url?: string; // Ссылка внешняя или внутренная, для внешней ещё надо указать external: true
    action?: () => void; // Передаём функцию на onClick
    style?: ButtonStyles;
    external?: boolean; // Внешняя ссылка с "target=_blank"
    checked?: boolean;
    disabled?: boolean;
    gaps?: number; // Левый отступ - 15, 20, 25, 30
    title?: string; // Тайтл на ховер
    dropDown?: boolean; // Кнопка в дропдауне
    splitDropDown?: boolean; // Если true - появляется справа кнопка со стрелочкой, которая вызывает дропдаут, если false - кнопка целиком вызывает дропдаун
    dropDownOpen?: boolean; // Если true - иконка стрелочки менятся на стрелку вверх
    size?: string; // middle, large
    formButton?: boolean;
    isLink?: boolean;
    hollow?: boolean; // Кнопка с бесцветным фоном и обводкой
    onKeyDown?: (event) => void;
    extraClass?: string;
    display?: 'block';
}

export class Button extends React.Component<ButtonProps, any>
{
    public setFocus()
    {
        setTimeout(() => {
            (ReactDOM.findDOMNode(this.refs['button']) as HTMLElement).focus();
        }, 50);
    }

    private onKeyDown(e)
    {
        this.props.onKeyDown && this.props.onKeyDown(e);
    }

    private renderIcon(): JSX.Element
    {
        if ( !this.props.icon ) {
            return null;
        }

        return(
            <i
                className={ classNames('toolbar__icon', this.props.icon) }
                style={ { color: this.props.iconColor } }
            />
        );
    }

    private renderText(): JSX.Element
    {
        let props = this.props;

        if ( !props.text ) {
            return null;
        }

        return(
            <span className={ classNames('toolbar__btn-text', props.icon && '_has-icon') }>
                { props.text }
                {
                    props.dropDown && !props.splitDropDown && !props.icon &&
                    <i className={ classNames('toolbar__btn-caret fa', props.dropDownOpen ? 'fa-caret-up' : 'fa-caret-down') } />
                }
            </span>
        );
    }

    private onClick(e: SyntheticEvent)
    {
        if ( this.props.action ) {
            this.props.action();
            e.preventDefault();
        }
    }

    renderButton(): JSX.Element
    {
        let data = this.props;
        let classes =
            classNames(
                'toolbar__btn',
                data.style ? '_style_' + data.style : '_style_default',
                data.disabled && '_state_disabled',
                data.checked && '_state_checked',
                data.isLink && data.url && !data.dropDown && '_type_normal',
                data.dropDown && '_type_drop-down',
                data.size && '_size_' + data.size,
                data.hollow && '_type_hollow',
                data.extraClass && data.extraClass,
                data.display && '_display_' + data.display,
                data.iconPosition && '_icon-position_' + data.iconPosition
            );

        if ( data.formButton ) {
            return(
                <button
                    className={ classes }
                    onClick={ (e) => this.onClick(e) }
                    disabled={ data.disabled }
                    onKeyDown={ (event) => {} }
                    ref="button"
                    type="submit"
                >
                    { this.renderContent() }
                </button>
            );
        } else {
            if ( data.url ) {
                if ( data.external ) {
                    return(
                        <a className={ classes } href={ data.url } target="_blank">
                            { this.renderContent() }
                        </a>
                    );
                } else {
                    return(
                        <Link tabIndex={ 0 } className={ classes } to={ data.url }>
                            { this.renderContent() }
                        </Link>
                    );
                }
            } else {
                return(
                    <a
                        tabIndex={ data.disabled ? -1 : 0 }
                        className={ classes }
                        onKeyDown={ (e) => this.onKeyDown(e) }
                        onClick={ (e) => this.onClick(e) }
                        ref="button"
                    >
                        { this.renderContent() }
                    </a>
                );
            }
        }
    }

    renderContent(): JSX.Element
    {
        return(
            <div>
                { this.props.iconPosition != 'right' && this.renderIcon() }
                { this.renderText() }
                { this.props.iconPosition == 'right' && this.renderIcon() }
            </div>
        );
    }

    render(): JSX.Element
    {
        let data = this.props;
        let classes =
            classNames(
                'toolbar__item',
                data.gaps && '_gaps_' + data.gaps,
                data.isLink && data.url && !data.dropDown && '_type_link',
                data.checked && '_state_checked',
                data.dropDown && '_type_drop-down',
                data.extraClass && data.extraClass,
                data.display && '_display_' + data.display
            );

        if ( data.dropDown ) {
            return this.renderButton();
        } else {
            return(
                <div title={ data.title } className={ classes }>
                    { this.renderButton() }
                </div>
            );
        }
    }
}