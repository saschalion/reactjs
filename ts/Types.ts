export enum KeyCodes {
    RIGHT = 39,
    LEFT = 37,
    UP = 38,
    DOWN = 40,
    ENTER = 13,
    ESC = 27,
    DEL = 46,
    SPACE = 32,
    TAB = 9,

    KEY_0 = 48,
    KEY_9 = 57
}