import React = require('react');

import { IndexRoute, hashHistory, Router, Route } from 'react-router';
import { Home } from './Pages/Home/Home';

class Main extends React.Component<any, any>
{
    public static createRouter()
    {
        return (
            <Router history={ hashHistory }>
                <Route path="/" component={ Main }>
                    <IndexRoute component={ Home } />

                    <Route path="*" component={ Home } />
                </Route>
            </Router>
        );
    }

    render(): JSX.Element
    {
        return this.props.children;
    }
}

export = Main;