export var RIGHT = 39;
export var LEFT = 37;
export var UP = 38;
export var DOWN = 40;
export var ENTER = 13;
export var ESC = 27;
export var DEL = 46;
export var SPACE = 32;
export var TAB = 9;

export var KEY_0 = 48;
export var KEY_1 = 49;
export var KEY_2 = 50;
export var KEY_3 = 51;
export var KEY_4 = 52;
export var KEY_5 = 53;
export var KEY_6 = 54;
export var KEY_7 = 55;
export var KEY_8 = 56;
export var KEY_9 = 57;

export var KEY_A = 65;