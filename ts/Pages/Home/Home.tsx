import classes = require('classnames');
import KeyboardEvent = __React.KeyboardEvent;
import React = require('react');

import { DropDown, DropDownItemData } from '../../Controls/UI/DropDown';

interface HomeState
{
    activeColorId?: string;
}

export class Home extends React.Component<any, HomeState>
{
    constructor()
    {
        super();
        this.state = {
            activeColorId: ''
        };
    }

    get items(): DropDownItemData[]
    {
        return [
            {
                id: '1',
                tag: '#859900',
                caption: 'Зелёный',
                group: 'З'
            },
            {
                id: '2',
                tag: '#fc0',
                caption: 'Жёлтый',
                group: 'Ж'
            },
            {
                id: '3',
                tag: '#002b36',
                caption: 'Чёрный',
                group: 'Ч'
            },
            {
                id: '4',
                tag: '#586e75',
                caption: 'Серый',
                group: 'С'
            },
            {
                id: '5',
                tag: '#cb4b16',
                caption: 'Красный',
                group: 'К'
            },
            {
                id: '6',
                tag: '#d33682',
                caption: 'Розовый',
                group: 'Р'
            },
            {
                id: '7',
                tag: '#6c71c4',
                caption: 'Фиолетовый',
                group: 'Ф'
            },
            {
                id: '8',
                tag: '#268bd2',
                caption: 'Синий',
                group: 'С'
            },
            {
                id: '9',
                tag: '#2aa198',
                caption: 'Лазурный',
                group: 'Л'
            },
            {
                id: '10',
                tag: '#859900',
                caption: 'Салатовый',
                group: 'С'
            },
        ];
    }

	render()
	{
        return(
            <div className="home">
                <div className="home__container">
                    <div className="home__fields">
                        <DropDown
                            caption="Цвет"
                            data={ this.items }
                            hasClearButton={ true }
                            id={ this.state.activeColorId }
                            onChange={ (id) => {
                                this.setState({
                                    activeColorId: id
                                });
                            } }
                            placeholder="Выбрать цвет"
                            hasSearch={ true }
                        />
                    </div>
                </div>
            </div>
        );
	}
}