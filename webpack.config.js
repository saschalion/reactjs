var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var WebpackNotifierPlugin = require('webpack-notifier');

module.exports = {
    entry: './index.js',
    output: {
        path: './dist',
        filename: 'main.js',
        publicPath: ''
    },
    devtool: 'source-map',
    resolve: {
        root: '.',
        modulesDirectories: ['node_modules', 'ts'],
        extensions: ['', '.ts', '.tsx', '.js']
    },
    module: {
        loaders: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader'
            }, {
                test: /\.sass$/,
                loader: ExtractTextPlugin.extract(
                    'css?sourceMap!sass?sourceMap&indentedSyntax'
                )
            }, {
                test: /\.(otf|ttf|woff|woff2|eot)(.*)$/,
                loader: "file-loader?name=[path][name].[ext]"
            }, {
                test: /\.(svg|png|jpg|gif|bmp)(.*)$/,
                loader: "file-loader?name=[path][name].[ext]"
            }, {
                test: /\.json$/,
                loader: "file-loader?name=[path][name].[ext]"
            }, {
                test: /\.ico$/,
                loader: "file-loader?name=[path][name].[ext]"
            }
        ]
    },
    sassLoader: {
        includePaths: [
            require('bourbon').includePaths
        ]
    },
    plugins: [
        new ExtractTextPlugin('style.css'),

        new webpack.DefinePlugin({'process.env': {
            'NODE_ENV': '"production"'
        }}),

        new WebpackNotifierPlugin({
            title: 'Сборка Reactjs'
        })
    ]
};